%%%=============================================================================
%%% @author V. S. Kondrahov <vkondrahov@akber-soft.ru>
%%% @copyright V. S. Kondrahov
%%% @version 1.0.0
%%% @end
%%%=============================================================================
%%% Module Description
%%% @doc <strong>Compiling:</strong> {@version}.<br/>
%%%  Unit test module for {@link game_serv}
%%% @end
%%%=============================================================================
-module(game_serv_SUITE).

-compile([export_all]).

-include_lib("common_test/include/ct.hrl").
-include_lib("eunit/include/eunit.hrl").
-include_lib("cross_zero.hrl").

-define(PROJECT, cross_zero).

all() ->
  [game_server,
   print_grid_test,
   check_diagonal_test,
   check_back_diagonal_test,
   check_line_test,
   check_vertical_test,
   check_array_on_win_test,
   is_short_win_test,
   is_in_array_test].

init_per_suite(Config) ->
  application: start(?PROJECT),
  Config.

end_per_suite(Config) ->
  application: stop(?PROJECT),
  Config.

game_server(_Config) ->
  game_serv: print_state_game(),
  Grid = [[?EMPTY, ?EMPTY, ?EMPTY],
          [?EMPTY, ?EMPTY, ?EMPTY],
          [?EMPTY, ?EMPTY, ?EMPTY]],
  Output = [[?EMPTY, ?EMPTY, ?EMPTY],
            [?EMPTY, ?CROSS, ?EMPTY],
            [?EMPTY, ?EMPTY, ?EMPTY]],
  Input_value = #call{axis = #axis{x = 2, y = 2}, value = ?CROSS},
  ?assertEqual(Output, game_serv: do_insert_value(Input_value, Grid)),
  game_serv: print_state_grid(),
  ok.

print_grid_test(_Config) ->
  Grid = [[?CROSS, ?ZERO, ?EMPTY],
          [?EMPTY, ?EMPTY, ?EMPTY],
          [?EMPTY, ?EMPTY, ?EMPTY]],
  Output = ["X O _ \n",
            "_ _ _ \n",
            "_ _ _ \n"],
  ?assertEqual(Output, utils: print_grid(Grid)),
  ok.

%% @doc
%% DIMENSION = 3,
%%
%% O _ _|_ _ _
%% _ O _|_ _ _
%% ____O|_ _ _
%% _ _ _ O _ _
%% _ _ _ _ O _
%% _ _ _ _ _ O
%% @end
check_diagonal_test(_Config) ->
  Cordinate = #axis{x = 3, y = 3},
  Grid = [[?ZERO, ?EMPTY, ?EMPTY],
          [?EMPTY, ?ZERO, ?EMPTY],
          [?EMPTY, ?EMPTY, ?ZERO]],
  ?assert(rules_win: check_diagonal(Cordinate, Grid)),
  Cordinate2 = #axis{x = 1, y = 1},
  Grid2 = [[?CROSS, ?EMPTY, ?EMPTY],
           [?EMPTY, ?CROSS, ?EMPTY],
           [?EMPTY, ?EMPTY, ?CROSS]],
  ?assert(rules_win: check_diagonal(Cordinate2, Grid2)),
  Cordinate3 = #axis{x = 2, y = 2},
  Grid3 = [[?ZERO, ?EMPTY, ?EMPTY],
           [?EMPTY, ?ZERO, ?EMPTY],
           [?EMPTY, ?EMPTY, ?ZERO]],
  ?assert(rules_win: check_diagonal(Cordinate3, Grid3)),
  Cordinate4 = #axis{x = 3, y = 3},
  Grid4 = [[?EMPTY, ?EMPTY, ?EMPTY],
           [?EMPTY, ?EMPTY, ?EMPTY],
           [?EMPTY, ?EMPTY, ?ZERO]],
  ?assertNot(rules_win: check_diagonal(Cordinate4, Grid4)),
  Cordinate5 = #axis{x = 3, y = 3},
  Grid5 = [],
  ?assertNot(rules_win: check_diagonal(Cordinate5, Grid5)).

check_back_diagonal_test(_Config) ->
  Cordinate = #axis{x = 3, y = 1},
  Grid = [[?ZERO, ?EMPTY, ?ZERO],
          [?EMPTY, ?ZERO, ?EMPTY],
          [?ZERO, ?EMPTY, ?EMPTY]],
  ?assert(rules_win: check_back_diagonal(Cordinate, Grid)),
  Cordinate2 = #axis{x = 1, y = 3},
  Grid2 = [[?CROSS, ?EMPTY,  ?CROSS],
           [?EMPTY, ?CROSS, ?EMPTY],
           [?CROSS, ?EMPTY, ?EMPTY]],
  ?assert(rules_win: check_back_diagonal(Cordinate2, Grid2)),
  Cordinate3 = #axis{x = 2, y = 2},
  Grid3 = [[?ZERO, ?EMPTY, ?ZERO],
           [?EMPTY, ?ZERO, ?EMPTY],
           [?ZERO, ?EMPTY, ?EMPTY]],
  ?assert(rules_win: check_back_diagonal(Cordinate3, Grid3)),
  Cordinate4 = #axis{x = 2, y = 3},
  Grid4 = [[?ZERO, ?EMPTY, ?EMPTY],
           [?EMPTY, ?EMPTY, ?EMPTY],
           [?EMPTY, ?EMPTY, ?ZERO]],
  ?assertNot(rules_win: check_back_diagonal(Cordinate4, Grid4)),
  Cordinate5 = #axis{x = 3, y = 3},
  Grid5 = [],
  ?assertNot(rules_win: check_back_diagonal(Cordinate5, Grid5)).

check_vertical_test(_Config) ->
  Cordinate = #axis{x = 2, y = 2},
  Grid = [[?ZERO, ?EMPTY, ?ZERO],
          [?ZERO, ?ZERO, ?ZERO],
          [?ZERO, ?EMPTY, ?EMPTY]],
  ?assertNot(rules_win: check_vertical(Cordinate, Grid)),
  Cordinate2 = #axis{x = 2, y = 1},
  Grid2 = [[?ZERO, ?CROSS, ?ZERO],
           [?EMPTY, ?CROSS, ?EMPTY],
           [?EMPTY, ?CROSS, ?CROSS]],
  ?assert(rules_win: check_vertical(Cordinate2, Grid2)),
  Cordinate3 = #axis{x = 3, y = 2},
  Grid3 = [[?ZERO, ?EMPTY, ?ZERO],
           [?EMPTY, ?ZERO, ?ZERO],
           [?ZERO, ?EMPTY, ?ZERO]],
  ?assert(rules_win: check_vertical(Cordinate3, Grid3)).

check_line_test(_Config) ->
  Cordinate = #axis{x = 2, y = 2},
  Grid = [[?ZERO, ?EMPTY, ?ZERO],
          [?ZERO, ?ZERO, ?ZERO],
          [?ZERO, ?EMPTY, ?EMPTY]],
  ?assert(rules_win: check_line(Cordinate, Grid)),
  Cordinate2 = #axis{x = 1, y = 1},
  Grid2 = [[?ZERO, ?ZERO, ?ZERO],
           [?EMPTY, ?ZERO, ?EMPTY],
           [?EMPTY, ?EMPTY, ?CROSS]],
  ?assert(rules_win: check_line(Cordinate2, Grid2)),
  Cordinate3 = #axis{x = 3, y = 1},
  Grid3 = [[?ZERO, ?EMPTY, ?ZERO],
           [?EMPTY, ?ZERO, ?EMPTY],
           [?ZERO, ?EMPTY, ?CROSS]],
  ?assertNot(rules_win: check_line(Cordinate3, Grid3)).

% I`m testing with Dimension_win = 3
% с символами 88, 79.
check_array_on_win_test(_Config) ->
  Arr  = [1, 1, 88, 88, 88, 88, 0, 0, 0],
  ?assert(rules_win: check_array_on_win(Arr)),
  Arr2  = [0, 0, 1, 1, 1, 0, 0, 0, 1],
  ?assertNot(rules_win: check_array_on_win(Arr2)),
  Arr3  = [0, 0, 1, 0, 0, 0, 88, 88, 88],
  ?assert(rules_win: check_array_on_win(Arr3)),
  Arr4  = [88, 88, 88, 0, 0, 0, 1, 1, 1],
  ?assert(rules_win: check_array_on_win(Arr4)),
  Arr5  = [88, 88, 0, 0, 0, 0, 79, 79, 1],
  ?assertNot(rules_win: check_array_on_win(Arr5)),
  Arr6 = [],
  ?assertNot(rules_win: check_array_on_win(Arr6)),
  Arr7  = [79, 79, 79, 0, 0, 0, 1, 1, 1],
  ?assert(rules_win: check_array_on_win(Arr7)).

% я проверял с символами 88, 79.
is_short_win_test(_Config) ->
  Arr  = [88, 88, 88, 88, 0, 0, 0],
  ?assert(rules_win: is_short_win(Arr)),
  Arr2  = [0, 0, 1, 0, 1, 0, 0, 0, 1],
  ?assertNot(rules_win: is_short_win(Arr2)),
  Arr3  = [0, 0, 0, 0, 0, 0, 0, 1, 1],
  ?assertNot(rules_win: is_short_win(Arr3)),
  Arr4  = [0, 0, 0, 0, 0, 0, 1, 1, 1],
  ?assertNot(rules_win: is_short_win(Arr4)),
  Arr5 = [],
  ?assertNot(rules_win: is_short_win(Arr5)).

is_in_array_test(_Config) ->
  ?assert(rules_win: is_in_array([3, 22, 4, 234, 1], [1])),
  ?assertNot(rules_win: is_in_array([3, 22, 4, 234, 1], [44])),
  ?assert(rules_win: is_in_array([3, 22, 4, 234, 1], [22, 4])),
  ?assert(rules_win: is_in_array("apple", "a")),
  ?assertNot(rules_win: is_in_array("apple", "z")).
