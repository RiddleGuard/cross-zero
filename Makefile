PROJECT = cross_zero
PROJECT_DESCRIPTION = Cross-zero
PROJECT_VERSION = 0.1.0

ERLC_OPTS = +debug_info

# test
EUNIT_OPTS = verbose
DIALYZER_OPTS = --verbose

include erlang.mk

SHELL_OPTS = -eval 'application: start($(PROJECT))'
ERLC_OPTS := $(filter-out -Werror,$(ERLC_OPTS))
