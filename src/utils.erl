-module(utils).

-compile([export_all]).

-include_lib("cross_zero.hrl").

-spec print_grid(grid()) -> Grid_list when
      Grid_list :: string().
print_grid(Grid) ->
  lists: map(fun(Line) when is_list(Line) ->
    ln(lists: map(fun(Value) when is_integer(Value) ->
        [[Value], ?SPACE]
      end, Line))
  end, Grid).

% ------------------------------------------------------------------------------
%% @doc get value call.
%% @throw function_clause
-spec get_value(cordinate(), grid()) -> state_call().

get_value(Cordinate, Grid) ->
  lists: nth(Cordinate#axis.x, lists: nth(Cordinate#axis.y, Grid)).

% ------------------------------------------------------------------------------
-spec try_get_value(cordinate(), grid()) -> state_call() | false.

try_get_value(Cordinate, Grid) ->
  try get_value(Cordinate, Grid)
  catch
    error: function_clause -> false
  end.

% ------------------------------------------------------------------------------
-spec ln(string()) -> List_out when
      List_out :: list().
ln(Str) ->
  lists: flatten([Str, ?CARRIAGE]).
