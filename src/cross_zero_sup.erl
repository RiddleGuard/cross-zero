-module(cross_zero_sup).
-behaviour(supervisor).

-export([init/1]).
-export([start_link/0]).

start_link() ->
  supervisor: start_link({local, ?MODULE}, ?MODULE, []).

init([]) ->
  Procs = [
    {serv,
      {game_serv, start_link, []},
      permanent,
      5000, % Время завершения
      worker,
      [game_serv]}],
  MaxRestart = 1, %6,
  MaxTime = 3600, %3600,
  {ok, {{one_for_one, MaxRestart, MaxTime}, Procs}}.
