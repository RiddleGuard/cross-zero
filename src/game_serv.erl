%%% ============================================================================
%%% @doc Этот модуль отражает состояние игры.
%%% Этот модуль также использует функцию модуля rules_win
%%% для того чтобы узнать кто победил.
%%% @end
%%% ============================================================================
-module(game_serv).

-behaviour(gen_server).

-include_lib("cross_zero.hrl").

-export([start_link/0]).
% users api
-export([
  init/1,
  handle_call/3,
  handle_cast/2,
  handle_info/2,
  terminate/2,
  code_change/3]).
% api game
-export([
  print_state_grid/0,
  print_state_game/0,
  insert_value/3,
  clear_grid/0]).
-export([
  do_insert_value/2]).

-define(SERVER, ?MODULE).

-record(state, {
  grid = [[]] :: [[string()]],
  state_game = start :: atom()
}).

% ==============================================================================
% Functions gen_servers
% ==============================================================================
start_link() ->
  gen_server: start_link({local, ?SERVER}, ?MODULE, {}, []).

print_state_grid() ->
  gen_server: call(?MODULE, print_state_grid).

print_state_game() ->
  gen_server: call(?MODULE, print_state_game).

insert_value(X, Y, State_call) ->
  gen_server: call(?MODULE, {insert_value, {call, {axis, X, Y}, State_call}}).

clear_grid() ->
  gen_server: call(?MODULE, clear_grid).

%% @private
%% @doc init array NxN.
init({}) ->
  Grid = [[?EMPTY || _X <- lists: seq(1, ?DIMENSION)]
    || _Y <- lists: seq(1, ?DIMENSION)],
  State = #state{
    grid = Grid,
    state_game = start},
  {ok, State}.

%% @private
handle_call(Request, _From, State) ->
  {Reply, NewState} =
    case Request of
      print_state_grid ->
        io: format("~s~n", [utils: print_grid(State#state.grid)]),
        {ok, State};
      print_state_game ->
        State_game = do_print_state_game(State#state.state_game),
        io: format("~s~n", [State_game]),
        {ok, State};
      {insert_value, {call, {axis, _X, _Y}, _State_call} = Value} ->
        Grid2 = do_insert_value(Value, State#state.grid),
        {Grid2, State#state{grid = Grid2}};
      clear_grid ->
        Grid2 = do_clear_grid(State#state.grid),
        {Grid2, State#state{grid = Grid2}}
    end,
  {reply, Reply, NewState}.

%% @private
handle_cast(_Msg, State) ->
  {noreply, State}.

%% @private
handle_info(_Info, State) ->
  {noreply, State}.

%% @private
terminate(_Reason, _State) ->
  ok.

%% @private
code_change(_OldVsn, State, _Extra) ->
  {ok, State}.

% ==============================================================================
% Internal functions gen_servers
% ==============================================================================
-spec do_print_state_game(State) -> binary() when
      State :: move_zero | move_cross | finish | start | error().
do_print_state_game(State) ->
    case State of
      move_zero ->
        <<"Zerro."/utf8>>;
      move_cross ->
        <<"Cross."/utf8>>;
      finish ->
        <<"the end."/utf8>>;
      start ->
        <<"start game."/utf8>>;
      _ ->
        <<"Error!!!"/utf8>>
    end.

% ------------------------------------------------------------------------------
-spec do_insert_value(call(), grid()) -> grid().

do_insert_value(Call, Grid) ->
  [[case X =:= Call#call.axis#axis.x andalso Y =:= Call#call.axis#axis.y of
      true -> Call#call.value;
      _ -> utils: get_value(#axis{x = X, y = Y}, Grid)
    end
  || X <- lists: seq(1, erlang: length(hd(Grid)))]
  || Y <- lists: seq(1, erlang: length(Grid))].

% ------------------------------------------------------------------------------
-spec do_clear_grid(In_grid) -> Out_grid when
      In_grid :: grid(),
      Out_grid :: grid().
do_clear_grid(Grid) ->
  lists: map(fun (Line) ->
    lists: map(fun (_Elem) -> ?EMPTY end, Line)
   end, Grid).
