-module(rules_win).

-include_lib("cross_zero.hrl").

-export([
  rule_win/2,
  check_diagonal/2,
  check_back_diagonal/2,
  check_vertical/2,
  check_line/2,
  check_array_on_win/1,
  is_short_win/1,
  is_in_array/2]).

% ------------------------------------------------------------------------------
-spec rule_win(call(), grid()) -> boolean().

rule_win(Call, Grid) ->
  check_line(Call#call.axis, Grid) and
  check_vertical(Call#call.axis, Grid) and
  check_diagonal(Call#call.axis, Grid) and
  check_back_diagonal(Call#call.axis, Grid).

% ------------------------------------------------------------------------------
%% @todo можно оптимизировать чтобы в функцию {@line check_array_on_win}
%% передовался масиив обрезанный от Cordinate по {-DIMENSION_WIN, DIMENSION_WIN}
%% так массив получится меньше и проходов соответственно меньше.
-spec check_line(cordinate(), grid()) -> boolean().

check_line(Cordinate, Grid) ->
  check_array_on_win(lists: nth(Cordinate#axis.y, Grid)).

% ------------------------------------------------------------------------------
-spec check_vertical(cordinate(), grid()) -> boolean().

check_vertical(Cordinate, Grid) ->
  check_array_on_win([lists: nth(Cordinate#axis.x, Line) || Line <- Grid]).

% ------------------------------------------------------------------------------
-spec check_back_diagonal(cordinate(), grid()) -> boolean().

check_back_diagonal(Cordinate, Grid) ->
  Cordinate2 = #axis{
    x = Cordinate#axis.x + ?DIMENSION_WIN,
    y = Cordinate#axis.y - ?DIMENSION_WIN},
  Iter = 2 * ?DIMENSION_WIN,
  check_back_diagonal(Cordinate2, Iter, Grid, []).

check_back_diagonal(_Cordinate, 0, _Grid, Acc) ->
  check_array_on_win(lists: reverse(Acc));
check_back_diagonal(Cordinate, Iter, Grid, Acc) ->
  Cordinate2 = #axis{
    x = Cordinate#axis.x - 1,
    y = Cordinate#axis.y + 1},
  Value = utils: try_get_value(Cordinate, Grid),
  % Iter2 = case Value of false -> 1; Value -> Iter - 1 end,
  check_back_diagonal(Cordinate2, Iter - 1, Grid, [Value | Acc]).

% ------------------------------------------------------------------------------
-spec check_diagonal(cordinate(), grid()) -> boolean().

check_diagonal(Cordinate, Grid) ->
  Cordinate2 = #axis{
    x = Cordinate#axis.x - ?DIMENSION_WIN,
    y = Cordinate#axis.y - ?DIMENSION_WIN},
  Iter = 2 * ?DIMENSION_WIN,
  check_diagonal(Cordinate2, Iter, Grid, []).

check_diagonal(_Cordinate, 0, _Grid, Acc) ->
  check_array_on_win(lists: reverse(Acc));
check_diagonal(Cordinate, Iter, Grid, Acc) ->
  Cordinate2 = #axis{
    x = Cordinate#axis.x + 1,
    y = Cordinate#axis.y + 1},
  Value = utils: try_get_value(Cordinate, Grid),
  % Iter2 = case Value of false -> 1; Value -> Iter - 1 end,
  check_diagonal(Cordinate2, Iter - 1, Grid, [Value | Acc]).

% ------------------------------------------------------------------------------
-spec check_array_on_win(list()) -> Is_win when
      Is_win :: boolean().

check_array_on_win(Arr) ->
  check_array_on_win(Arr, []).

check_array_on_win([], _Acc) ->
  false;
check_array_on_win([_Elem], _Acc) ->
  false;
check_array_on_win([H | T], Acc) ->
  try is_short_win([H | T]) of
    true -> true;
    _ -> check_array_on_win(T, [H | Acc])
  catch
    error: badarg ->
      false
  end.

% ------------------------------------------------------------------------------
-spec is_short_win(list()) -> Is_short_win when
  Is_short_win :: boolean().

is_short_win(Arr) ->
  is_short_win(Arr, ?DIMENSION_WIN, [?CROSS, ?ZERO]).

is_short_win(_Arr, 1, _Allowed_chars) ->
  true;
is_short_win([], _Dimension_win, _Allowed_chars) ->
  false;
is_short_win([H | T], Dimension_win, Allowed_chars) ->
  try H =:= hd(T) andalso is_in_array(Allowed_chars, [H]) of
    true -> is_short_win(T, Dimension_win - 1, Allowed_chars);
    _ -> false
  catch
    error: Error when Error =:= badarg orelse Error =:= undef ->
      false
  end.

% ------------------------------------------------------------------------------
-spec is_in_array(Arr, Char) -> Is_chars when
      Char :: char(),
      Arr :: list(),
      Is_chars :: boolean().

is_in_array(_, []) ->
  false;
is_in_array(Arr, [Char | _]) ->
  lists: member(Char, Arr).
