-define(EMPTY, 95).
-define(CROSS, 88).
-define(ZERO, 79).
-define(SPACE, 32).
-define(CARRIAGE, 10).
-define(DIMENSION, 3).
-define(DIMENSION_WIN, 3).
-define(LINE_DASHES, ["-------\n"]).

% it`s for degging.
-define(DBG(Value), (fun() ->
    io: format("~p~n", [Value]),
    Value
  end)()).

-record(axis, {
  x :: dimension(),
  y :: dimension()
}).
-record(call, {
  axis :: cordinate(),
  value :: state_call()
}).

% common
-type state_call() :: ?ZERO | ?CROSS | ?EMPTY.
-type grid() :: [line()].
-type line() :: [state_call()].
-type dimension() :: 1..?DIMENSION.
-type error() :: atom().
% users
-type call() :: #call{}.
-type cordinate() :: #axis{}.
